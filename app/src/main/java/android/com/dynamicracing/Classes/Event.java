package android.com.dynamicracing.Classes;

public class Event {
    String eventName;
    String currentPlayerNumber;

    public Event(){

    }

    public Event(String eventName, String currentPlayerNumber) {
        this.eventName = eventName;
        this.currentPlayerNumber = currentPlayerNumber;
    }
}
