package android.com.dynamicracing.Classes;

public class Users {
   public String firstName,lastName ,nickName, age, email, password, phoneNumber, gender, profilePic, rank, leaderBoardID, queueOfRace, userID;

    public Users(){
    }

    public Users(String firstName, String lastName, String nickName, String age, String email, String password, String phoneNumber, String gender, String profilePic, String rank, String leaderBoardID, String queueOfRace, String userID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.age = age;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.profilePic = profilePic;
        this.rank = rank;
        this.leaderBoardID = leaderBoardID;
        this.queueOfRace = queueOfRace;
        this.userID = userID;
    }
}
