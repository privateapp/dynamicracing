package android.com.dynamicracing.UserActivity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.com.dynamicracing.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryRanksOfUserFragment extends Fragment {


    public HistoryRanksOfUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.user_history_ranks_fragment, container, false);
    }

}
