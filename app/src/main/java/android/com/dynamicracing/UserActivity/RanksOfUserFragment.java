package android.com.dynamicracing.UserActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.com.dynamicracing.AdminActivity.RanksOfAdminFragment;
import android.com.dynamicracing.Classes.Users;
import android.com.dynamicracing.Global.AppService;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.com.dynamicracing.R;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class RanksOfUserFragment extends Fragment {
    View view;
    ListView listView;
    CustomAdapter customAdapter;
    Button btnWantToRace;
    TextView txtMyNumber;

    String lastRank = "";
    int myNumber = 0;
    public RanksOfUserFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.user_ranks_fragment, container, false);

        initViews();
        setViewsListeners();

        customAdapter = new CustomAdapter();
        updateListView();
        return view;
    }

    void initViews(){
        listView = view.findViewById(R.id.listView);
        btnWantToRace = view.findViewById(R.id.btnWantToRace);
        txtMyNumber = view.findViewById(R.id.txtMyNumber);

        checkIfUserHasQueueNumber();
        checkIfUserPannedFromCurrentRace();
    }

    void checkIfUserHasQueueNumber(){
        DocumentReference user = AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().getStringFromSharedPreferences("Username"));
        user.get().addOnCompleteListener(new OnCompleteListener< DocumentSnapshot >() {
            @Override
            public void onComplete(@NonNull Task< DocumentSnapshot > task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    if(doc.getString("queueOfRace").equalsIgnoreCase("0")){
                       btnWantToRace.setVisibility(View.VISIBLE);
                       txtMyNumber.setVisibility(View.GONE);
                       myNumber = 0;
                   }else {
                       btnWantToRace.setVisibility(View.GONE);
                       txtMyNumber.setVisibility(View.VISIBLE);
                       txtMyNumber.setText("My Number: " + doc.getString("queueOfRace"));
                       myNumber = Integer.parseInt(doc.getString("queueOfRace"));
                       checkIfUserQueueNumberPassedWithoutPlay();
                   }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }

    void checkIfUserQueueNumberPassedWithoutPlay(){
        AppService.getInstance().mFireStore.collection("Events").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                for (DocumentSnapshot snapshot : documentSnapshots){
                    if(Integer.parseInt(snapshot.getString("currentPlayerNumber")) > myNumber){
                        // Reset User queue number
                        DocumentReference contact = AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().getStringFromSharedPreferences("Username"));
                        contact.update("queueOfRace", "0").addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                btnWantToRace.setVisibility(View.VISIBLE);
                                txtMyNumber.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            }
        });
    }

    void checkIfUserPannedFromCurrentRace(){
        AppService.getInstance().mFireStore.collection("Users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                for (DocumentSnapshot snapshot : documentSnapshots){
                    if(snapshot.getString("queueOfRace").equalsIgnoreCase("-1")
                            && snapshot.getString("email").equalsIgnoreCase(AppService.getInstance().getStringFromSharedPreferences("Username"))){ // -1 means that can't play current race
                        btnWantToRace.setVisibility(View.GONE);
                        txtMyNumber.setVisibility(View.GONE);
                        myNumber = 0;
                    }
                }
            }
        });
    }

    void setViewsListeners(){
        btnWantToRace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppService.getInstance().mFireStore.collection("Users").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                if(Integer.parseInt(document.getString("queueOfRace")) > myNumber){
                                    myNumber = Integer.parseInt(document.getString("queueOfRace"));
                                    Log.d("RanksOfUsers", "btnWantToRace onClick: " + document.getString("email") + " -- " +  document.getString("queueOfRace"));
                                }
                            }
                            myNumber +=1;
                            btnWantToRace.setVisibility(View.GONE);
                            txtMyNumber.setText("My Number: " + myNumber);
                            txtMyNumber.setVisibility(View.VISIBLE);
                            // Update data
                            DocumentReference contact = AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().getStringFromSharedPreferences("Username"));
                            contact.update("queueOfRace", String.valueOf(myNumber)).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                }
                            });
                        } else {
//                                    Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
            }
        });
    }

    void updateListView(){
        AppService.getInstance().mFireStore.collection("Users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                AppService.getInstance().racerMap.clear();
                for (DocumentSnapshot snapshot : documentSnapshots){
                    if(!snapshot.getString("email").equalsIgnoreCase("app@dynamicssimulators.com")) {
                        Users users = new Users();
                        users.nickName = snapshot.getString("nick_name");
                        users.userID = snapshot.getString("userID");
                        users.rank = snapshot.getString("rank");
                        if(snapshot.getString("email").equalsIgnoreCase(AppService.getInstance().getStringFromSharedPreferences("Username")))
                        if(!lastRank.equalsIgnoreCase(snapshot.get("rank").toString()) && !snapshot.get("rank").toString().equalsIgnoreCase("0")){
                            lastRank = snapshot.get("rank").toString();
                            pushNotificationOnDBChanges();
                        }
                        AppService.getInstance().racerMap.put(snapshot.getString("email"),users);
                    }
                }
                AppService.getInstance().racerMap = sortByValue(AppService.getInstance().racerMap);
                listView.setAdapter(customAdapter);
                customAdapter.notifyDataSetChanged();
            }
        });
    }

    void pushNotificationOnDBChanges(){
        NotificationManager notif=(NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify=new Notification.Builder
                (getApplicationContext()).setContentTitle("Update").setContentText("Your ranks has been changed!").setSmallIcon(R.drawable.car_logo).setAutoCancel(true).build();

        notif.notify(0, notify);
    }

    private Map<String, Users> sortByValue(Map<String, Users> unsortMap) {
        // 1. Convert Map to List of Map
        List<Map.Entry<String, Users>> list =
                new LinkedList<Map.Entry<String, Users>>(unsortMap.entrySet());
        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Users>>() {
            public int compare(Map.Entry<String, Users> o1,
                               Map.Entry<String, Users> o2) {
                return (o1.getValue().rank).compareTo(o2.getValue().rank);
            }
        });
        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Users> sortedMap = new LinkedHashMap<String, Users>();
        for (Map.Entry<String, Users> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        /*
        //classic iterator example
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }*/
        return sortedMap;
    }

    class CustomAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        Object[] usersIDArray;
        public CustomAdapter()
        {
            mInflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            int sizeOfUsersWithRanks = 0;
            for (Map.Entry<String, Users> entry : AppService.getInstance().racerMap.entrySet()) {
                if(!entry.getValue().rank.equalsIgnoreCase("0")){
                    sizeOfUsersWithRanks +=1;
                }
            }
            usersIDArray = new String[sizeOfUsersWithRanks];
            int indexOfusersIDArray = 0;
            for (Map.Entry<String, Users> entry : AppService.getInstance().racerMap.entrySet()) {
                if(!entry.getValue().rank.equalsIgnoreCase("0")) {
                    usersIDArray[indexOfusersIDArray] = entry.getKey();
                    indexOfusersIDArray += 1;
                }
            }
            // TODO Auto-generated method stub
            return usersIDArray.length;//listview item count.
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            convertView = mInflater.inflate(R.layout.custom_row_layout_of_listview, parent,false);
            //inflate custom layout
            RelativeLayout rloRowContainer = (RelativeLayout) convertView.findViewById(R.id.rloRowContainer);
            TextView txtNumOfUser= (TextView)convertView.findViewById(R.id.txtNumOfUser);
            TextView txtNickName= (TextView)convertView.findViewById(R.id.txtNickName);
            TextView txtID= (TextView)convertView.findViewById(R.id.txtID);
            TextView txtTime= (TextView)convertView.findViewById(R.id.txtTime);
            TextView txtEmail= (TextView)convertView.findViewById(R.id.txtEmail);

            txtID.setVisibility(View.GONE);
            txtEmail.setVisibility(View.GONE);

            txtNumOfUser.setText(position+1 + "- ");
            txtNickName.setText(AppService.getInstance().racerMap.get(usersIDArray[position]).nickName);
            txtID.setText(AppService.getInstance().racerMap.get(usersIDArray[position]).userID);
            txtTime.setText("Time: " + AppService.getInstance().racerMap.get(usersIDArray[position]).rank);
            txtEmail.setText("Email: " + usersIDArray[position].toString());

            return convertView;
        }
    }
}
