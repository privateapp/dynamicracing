package android.com.dynamicracing.UserActivity;

import android.com.dynamicracing.AdminActivity.InfoOfAdminFragment;
import android.com.dynamicracing.AdminActivity.RanksOfAdminFragment;
import android.com.dynamicracing.R;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

public class UserActivity extends AppCompatActivity {
    private FrameLayout mainFrameLayout;
    BottomNavigationView bottomNavigation;

    private RanksOfUserFragment ranksOfUserFragment;
    private ProfileOfUserFragment profileOfUserFragment;
    private HistoryRanksOfUserFragment historyRanksOfUserFragment;
    private AboutUsFragment aboutUsFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);
        initViews();
        setViewsAction();
        setFragment(ranksOfUserFragment);
    }
    void initViews(){
        mainFrameLayout = findViewById(R.id.mainFrameLayout);

        ranksOfUserFragment = new  RanksOfUserFragment();
        profileOfUserFragment = new ProfileOfUserFragment();
        historyRanksOfUserFragment = new HistoryRanksOfUserFragment();
        aboutUsFragment = new AboutUsFragment();

        bottomNavigation = findViewById(R.id.bottomNavigationView);
    }

    void setViewsAction(){
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.user_navigation_ranks:
                        setFragment(ranksOfUserFragment);
                        return true;
                    case R.id.user_navigation_profile:
                        setFragment(profileOfUserFragment);
                        return true;
                    case R.id.user_navigation_about:
                        setFragment(aboutUsFragment);
                        return true;
                }
                return false;
            }
        });
    }

    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.commit();
    }
}
