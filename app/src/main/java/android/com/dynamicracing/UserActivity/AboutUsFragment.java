package android.com.dynamicracing.UserActivity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.com.dynamicracing.R;
import android.widget.ImageButton;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutUsFragment extends Fragment {
    ImageButton ibtnFacebookFollow, ibtnInstaFollow, ibtnInstaRamyFollow;
    View view;

    public AboutUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.user_about_us_fragment, container, false);

        ibtnFacebookFollow = view.findViewById(R.id.ibtnFacebookFollow);
        ibtnInstaFollow = view.findViewById(R.id.ibtnInstaFollow);
        ibtnInstaRamyFollow = view.findViewById(R.id.ibtnInstaRamyFollow);

        ibtnFacebookFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.facebook.com/DynamicsSim/"));
                startActivity(browserIntent);
            }
        });
        ibtnInstaFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/dynamicssim/"));
                startActivity(browserIntent);
            }
        });
        ibtnInstaRamyFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/rami_serry_academy/"));
                startActivity(browserIntent);
            }
        });

        return view;
    }

}
