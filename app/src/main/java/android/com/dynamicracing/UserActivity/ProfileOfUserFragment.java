package android.com.dynamicracing.UserActivity;


import android.app.Notification;
import android.app.NotificationManager;
import android.com.dynamicracing.Global.AppService;
import android.com.dynamicracing.Login.LoginActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.com.dynamicracing.R;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileOfUserFragment extends Fragment {
    View view;
    TextView txtName, txtEmail, txtID, txtRank;
    Button btnSignOut;

    public ProfileOfUserFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.user_profile_fragment, container, false);

        txtName = (TextView)view.findViewById(R.id.txtName);
        txtEmail = (TextView)view.findViewById(R.id.txtEmail);
        txtID = (TextView)view.findViewById(R.id.txtID);
        txtRank = (TextView)view.findViewById(R.id.txtRank);
        btnSignOut = view.findViewById(R.id.btnSignOut);

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToLoginScreenAndLogout();
            }
        });

        readSingleContact();
        return view;
    }

    void backToLoginScreenAndLogout(){
        AppService.getInstance().mFirebaseAuth.signOut();
        startActivity(new Intent(getActivity(), LoginActivity.class));
    }

    private void readSingleContact() {
        DocumentReference user = AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().getStringFromSharedPreferences("Username"));
        user.get().addOnCompleteListener(new OnCompleteListener< DocumentSnapshot >() {
            @Override
            public void onComplete(@NonNull Task< DocumentSnapshot > task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    txtName.setText("Name: "+ doc.get("nick_name"));
                    txtEmail.setText("Email: "+ doc.get("email"));
                    txtID.setText("ID: "+ doc.get("userID"));
                    txtRank.setText("Rank: "+ doc.get("rank"));
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }


}
