package android.com.dynamicracing.Global;

import android.com.dynamicracing.AdminActivity.AdminActivity;
import android.com.dynamicracing.Classes.Users;
import android.com.dynamicracing.Login.LoginActivity;
import android.com.dynamicracing.Login.LoginFragment;
import android.com.dynamicracing.UserActivity.UserActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AppService {
    private static AppService appServiceObject;

    // Facebook
    public CallbackManager callbackManager;
    public AccessToken facebookAccessToken;

    // Firebase
    public FirebaseFirestore mFireStore;
    public FirebaseAuth mFirebaseAuth;
    public FirebaseUser mFirebaseUser;
    public DocumentReference mDocumentReference;
    // Activity
    public LoginActivity loginActivity;
    // Fragments
    public LoginFragment loginFragment;
    // Variables
    public Map<String, String> userMap;
    public Map<String, Users> racerMap;
    public SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private AppService(){
        loginActivity = new LoginActivity();
        mFireStore = FirebaseFirestore.getInstance();
        mFirebaseAuth = FirebaseAuth.getInstance();
        userMap = new HashMap<>();
        racerMap = new HashMap<>();
    }

    public static AppService getInstance(){
        if(appServiceObject == null){
            appServiceObject = new AppService();
            Log.w("AppService", "AppService --> getInstance() called");
        }
        return appServiceObject;
    }

    public void saveStringIntoSharedPreferences(String key, String value){
        if(editor == null) {
            editor = sharedpreferences.edit();
        }
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringFromSharedPreferences(String key){
        return sharedpreferences.getString(key, "");
    }

    public void saveIntegerIntoSharedPreferences(String key, int value){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getIntFromSharedPreferences(String key){
        return sharedpreferences.getInt(key, 0);
    }

    public void checkIfUserAlreadyLoggedIn(Context context) {
        if (AppService.getInstance().isNetworkAvailable(context)) {
            if (AppService.getInstance().mFirebaseAuth.getCurrentUser() != null) {
                mFirebaseUser = AppService.getInstance().mFirebaseAuth.getCurrentUser();
                if (AppService.getInstance().mFirebaseAuth.getCurrentUser().getEmail().equalsIgnoreCase(AppService.getInstance().getStringFromSharedPreferences("Username"))) {
                    // Handle the already login user
                    if(AppService.getInstance().mFirebaseAuth.getCurrentUser().getEmail().equalsIgnoreCase("app@dynamicssimulators.com")){
                        context.startActivity(new Intent(context, AdminActivity.class));
                    }else {
                        context.startActivity(new Intent(context, UserActivity.class));
                    }
                } else {
                    // check if a user is logged in via Facebook in Firebase Auth
                    for (UserInfo userInfo : mFirebaseUser.getProviderData()) {
                        if (userInfo.getProviderId().equals("facebook.com")) {
                            Log.d("AppService", "checkIfUserAlreadyLoggedIn() --> User is signed in with Facebook");
                            loginFragment.checkIfFacebookMailExistsInDB();
//                            context.startActivity(new Intent(context, MainActivity.class));
                            break;
                        }
                    }
                }
            }
        } else {
            Toast.makeText(context, "Please check your internet connection to login!", Toast.LENGTH_LONG).show();
        }
    }
    public boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*-----------------------------------Event fired if network back again---------------------------------------------------*/
    public void signInIfNetworkIsBecameAvailable(Context context){
        String BROADCAST_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                AppService.getInstance().checkIfUserAlreadyLoggedIn(context);
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);

        context.registerReceiver(receiver, filter);
    }
}
