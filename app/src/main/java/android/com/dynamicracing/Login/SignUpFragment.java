package android.com.dynamicracing.Login;

import android.com.dynamicracing.Global.AppService;
import android.com.dynamicracing.Global.Tools;
import android.com.dynamicracing.R;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.DocumentReference;
import com.hbb20.CountryCodePicker;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SignUpFragment extends Fragment {
    View view;
    ImageButton ibtnBack, ibtnReadTerms;
    Button btnSignUp;
    private EditText eTxtRegFirstName, eTxtRegLastName, eTxtRegNickName, eTxtRegAge, eTxtRegEmail, eTxtRegPassword, eTxtRegConfirmPassword, eTxtRegPhoneNumber;
    private CountryCodePicker countryCodePicker;
    private RadioGroup rbgRegGender;
    private CheckBox checkBoxTerms;

    String strName = "- Dynamics reserves the right to disqualify players for misconduct, foul language, behavior, or rough handling/vandalism of equipment." +
            "\n\n- Dynamics also reserves the right to disqualify players after 5 minutes of being notified for turn." +
            "\n\n- Players must show Insomnia ticket in order to play." +
            "\n\n- Email must be verified in order to win any prize from Dynamics." +
            "\n\n- Children under 16 must have parent present to receive prize." +
            "\n\n- Player must be able to reach pedals in order to enter the race." +
            "\n\n- Children may not sit on parent lap during race.";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sign_up_fragment, container, false);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            initViews();
            setViewsListeners();
        }
    }

    void initViews(){
        // Buttons
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnSignUp = view.findViewById(R.id.btnSignUp);
        ibtnReadTerms = view.findViewById(R.id.ibtnReadTerms);
        // EditText
        eTxtRegFirstName = view.findViewById(R.id.eTxtRegFirstName);
        eTxtRegLastName = view.findViewById(R.id.eTxtRegLastName);
        eTxtRegNickName = view.findViewById(R.id.eTxtRegNickName);
        eTxtRegAge = view.findViewById(R.id.eTxtRegAge);
        eTxtRegEmail = view.findViewById(R.id.eTxtRegEmail);
        eTxtRegPassword = view.findViewById(R.id.eTxtRegPassword);
        eTxtRegConfirmPassword = view.findViewById(R.id.eTxtRegConfirmPassword);
        eTxtRegPhoneNumber = view.findViewById(R.id.eTxtRegPhoneNumber);
        eTxtRegFirstName = view.findViewById(R.id.eTxtRegFirstName);
        // Country code
        countryCodePicker = view.findViewById(R.id.countryCodePicker);
        // RadioGroup
        rbgRegGender = view.findViewById(R.id.rbgRegGender);

        // Checkbox
        checkBoxTerms = view.findViewById(R.id.checkBoxTerms);
        checkIfUserInfoExistsFromSocialAccount();
    }

    void checkIfUserInfoExistsFromSocialAccount(){
        if(AppService.getInstance().mFirebaseUser != null) {
            Log.w("LoginFragment", "checkIfUserInfoExistsFromSocialAccount() --> Facebook Email: " + AppService.getInstance().mFirebaseUser.getEmail());
            eTxtRegEmail.setText(AppService.getInstance().mFirebaseUser.getEmail());
            eTxtRegNickName.setText(AppService.getInstance().mFirebaseUser.getDisplayName());
        }
    }

    void setViewsListeners(){
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LoginActivity) getActivity()).changeFragment(AppService.getInstance().loginActivity.LOGIN_FRAGMENT_INDEX);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNewUser();
            }
        });
        ibtnReadTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTermsDialog();
            }
        });
    }

    void showTermsDialog(){
        AlertDialog.Builder builderInner = new AlertDialog.Builder(getActivity());
        builderInner.setMessage(strName);
        builderInner.setTitle("Terms & Conditions");
        builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog,int which) {
                dialog.dismiss();
            }
        });
        builderInner.show();
    }

    void addNewUser(){
        validateUserInput();
    }

    void validateUserInput(){
        final String firstName = eTxtRegFirstName.getText().toString().trim();
        final String lastName = eTxtRegLastName.getText().toString().trim();
        final String nickName = eTxtRegNickName.getText().toString().trim();
        final String age = eTxtRegAge.getText().toString().trim();
        final String email = eTxtRegEmail.getText().toString().trim();
        final String password = eTxtRegPassword.getText().toString().trim();
        final String phoneNumber = countryCodePicker.getSelectedCountryCodeWithPlus().trim() + eTxtRegPhoneNumber.getText().toString().trim();
        final String gender = ((RadioButton) view.findViewById(rbgRegGender.getCheckedRadioButtonId())).getText().toString().trim();

        if(firstName.isEmpty()){
            eTxtRegFirstName.setError("Name required");
            eTxtRegFirstName.requestFocus();
            return;
        }
        if(lastName.isEmpty()){
            eTxtRegLastName.setError("Name required");
            eTxtRegLastName.requestFocus();
            return;
        }
        if(nickName.isEmpty()){
            eTxtRegNickName.setError("Name required");
            eTxtRegNickName.requestFocus();
            return;
        }
        if(age.isEmpty()){
            eTxtRegAge.setError("Age required");
            eTxtRegAge.requestFocus();
            return;
        }

        if(Integer.parseInt(age) < 10 ){
            eTxtRegAge.setError("Your age should be 10 or above years old!");
            eTxtRegAge.requestFocus();
            return;
        }

        if(email.isEmpty()){
            eTxtRegEmail.setError("Email required");
            eTxtRegEmail.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            eTxtRegEmail.setError("Email required");
            eTxtRegEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            eTxtRegPassword.setError("Password required");
            eTxtRegPassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            eTxtRegPassword.setError("Password should be at least 6 characters");
            eTxtRegPassword.requestFocus();
            return;
        }
        if (eTxtRegConfirmPassword.getText().toString().isEmpty()) {
            eTxtRegConfirmPassword.setError("Password confirmation required");
            eTxtRegConfirmPassword.requestFocus();
            return;
        }
        if (!eTxtRegConfirmPassword.getText().toString().equals(password)) {
            eTxtRegConfirmPassword.setError("Password confirmation doesn't match with password");
            eTxtRegConfirmPassword.requestFocus();
            return;
        }

        if(phoneNumber.isEmpty()) {
            eTxtRegPhoneNumber.setError("Phone number required");
            eTxtRegPhoneNumber.requestFocus();
            return;
        }

        if(!checkBoxTerms.isChecked()){
            checkBoxTerms.setError("You must agree our terms to SignUp");
            checkBoxTerms.requestFocus();
            return;
        }

        AppService.getInstance().userMap.clear();
        AppService.getInstance().userMap.put("nick_name",nickName);
        AppService.getInstance().userMap.put("first_name",firstName);
        AppService.getInstance().userMap.put("last_name",lastName);
        AppService.getInstance().userMap.put("last_name",lastName);
        AppService.getInstance().userMap.put("age",age);
        AppService.getInstance().userMap.put("email",email.toLowerCase());
        AppService.getInstance().userMap.put("password", Tools.md5(password));
        AppService.getInstance().userMap.put("phone_number",phoneNumber);
        AppService.getInstance().userMap.put("gender",gender);
        AppService.getInstance().userMap.put("rank", "0");
        AppService.getInstance().userMap.put("leaderBoardID", "1");
        AppService.getInstance().userMap.put("queueOfRace", "0");
        AppService.getInstance().userMap.put("userID", "0");

        authEmailAndPassword();
    }

    void authEmailAndPassword(){
        boolean isUserHasProvider = false;
        // check if a user is logged in via Facebook in Firebase Auth
        if(AppService.getInstance().mFirebaseUser != null) {
            for (UserInfo userInfo : AppService.getInstance().mFirebaseUser.getProviderData()) {
                if (userInfo.getProviderId().equals("facebook.com")) {
                    Log.d("AppService", "checkIfUserAlreadyLoggedIn() --> User is signed in with Facebook");
                    insertDataIntoFireStore();
                    isUserHasProvider = true;
                    break;
                }
            }
        }
        if(!isUserHasProvider) {
            AppService.getInstance().mFirebaseAuth.createUserWithEmailAndPassword(AppService.getInstance().userMap.get("email"), AppService.getInstance().userMap.get("password")).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    Toast.makeText(getContext(), "Email authenticated successfully!", Toast.LENGTH_SHORT).show();
                    insertDataIntoFireStore();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    void insertDataIntoFireStore(){
        if(AppService.getInstance().mFireStore != null)
            AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().userMap.get("email")).set(AppService.getInstance().userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // Set user id
                    DocumentReference updateUserID = AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().userMap.get("email"));
                    updateUserID.update("userID", AppService.getInstance().mFirebaseAuth.getUid()).addOnSuccessListener(new OnSuccessListener < Void > () {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getContext(), "Account created successfully!", Toast.LENGTH_SHORT).show();
//                            AppService.getInstance().mFirebaseUser = AppService.getInstance().mFirebaseAuth.getCurrentUser();
                            backToLoginScreenAndLogout();
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
    }

    void backToLoginScreenAndLogout(){
        AppService.getInstance().mFirebaseAuth.signOut();
        ((LoginActivity) getActivity()).changeFragment(AppService.getInstance().loginActivity.LOGIN_FRAGMENT_INDEX);
    }
}
