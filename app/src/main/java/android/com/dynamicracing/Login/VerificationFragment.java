package android.com.dynamicracing.Login;

import android.com.dynamicracing.Global.AppService;
import android.com.dynamicracing.R;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class VerificationFragment extends Fragment {
    View view;
    ImageButton ibtnBack;
    Button btnSendVerificationEmail;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.verification_fragment, container, false);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
//            sendUserEmailVerification();
//            initViews();
//            setViewsListeners();
        }
    }

    void initViews(){
        ibtnBack = view.findViewById(R.id.ibtnBack);
        btnSendVerificationEmail = view.findViewById(R.id.btnSendVerificationEmail);
    }

    void setViewsListeners(){
        btnSendVerificationEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendUserEmailVerification();
            }
        });
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppService.getInstance().mFirebaseAuth.signOut();
                ((LoginActivity) getActivity()).changeFragment(AppService.getInstance().loginActivity.LOGIN_FRAGMENT_INDEX);
            }
        });
    }

    void sendUserEmailVerification(){
        if(AppService.getInstance().mFirebaseUser != null) {
            AppService.getInstance().mFirebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(getContext(), "Verification Email has been sent!", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getContext(), "Verification Email not sent!", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }
}
