package android.com.dynamicracing.Login;

import android.com.dynamicracing.Global.AppService;
import android.com.dynamicracing.R;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;

public class LoginActivity extends AppCompatActivity {
     final int LOGIN_FRAGMENT_INDEX = 0;
     final int SIGNUP_FRAGMENT_INDEX = 1;
    final int VERFICATION_EMAIL_FRAGMENT_INDEX = 2;
     final int RESET_PASSWORD_FRAGMENT_INDEX = 3;

    private CustomViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        // Facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppService.getInstance().callbackManager = CallbackManager.Factory.create();

        AppService.getInstance().sharedpreferences = getSharedPreferences("SharedPreferences", MODE_PRIVATE); // 0 - for private mode

        viewPager = findViewById(R.id.container);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setPagingEnabled(false);
        changeFragment(LOGIN_FRAGMENT_INDEX);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        AppService.getInstance().checkIfUserAlreadyLoggedIn(this);
//        AppService.getInstance().signInIfNetworkIsBecameAvailable(this);
    }


    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == LOGIN_FRAGMENT_INDEX)
            super.onBackPressed();
        else {
            AppService.getInstance().mFirebaseAuth.signOut();
            changeFragment(LOGIN_FRAGMENT_INDEX);
        }
    }

    void changeFragment(int fragmentType) {
        Log.w("LoginActivity", "changeFragment() --> fragmentType: " + fragmentType);
        if(viewPager != null) {
            if(fragmentType == LOGIN_FRAGMENT_INDEX){
                viewPager.setCurrentItem(LOGIN_FRAGMENT_INDEX);
            }else if(fragmentType == SIGNUP_FRAGMENT_INDEX){
                viewPager.setCurrentItem(SIGNUP_FRAGMENT_INDEX);
            }
            else if(fragmentType == VERFICATION_EMAIL_FRAGMENT_INDEX){
                viewPager.setCurrentItem(VERFICATION_EMAIL_FRAGMENT_INDEX);
            }
            else if(fragmentType == RESET_PASSWORD_FRAGMENT_INDEX){
                viewPager.setCurrentItem(RESET_PASSWORD_FRAGMENT_INDEX);
            }
        }
    }
}
