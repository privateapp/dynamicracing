package android.com.dynamicracing.Login;

import android.com.dynamicracing.Global.AppService;
import android.com.dynamicracing.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class ResetPasswordFragment extends Fragment {
    View view;
    ImageButton ibtnBack;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.reset_password_fragment, container, false);

        ibtnBack = view.findViewById(R.id.ibtnBack);
        ibtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LoginActivity) getActivity()).changeFragment(AppService.getInstance().loginActivity.LOGIN_FRAGMENT_INDEX);
            }
        });

        return view;
    }
}
