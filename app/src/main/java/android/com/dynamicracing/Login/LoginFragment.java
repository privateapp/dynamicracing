package android.com.dynamicracing.Login;

import android.com.dynamicracing.AdminActivity.AdminActivity;
import android.com.dynamicracing.Global.AppService;
import android.com.dynamicracing.Global.Tools;
import android.com.dynamicracing.R;
import android.com.dynamicracing.UserActivity.UserActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Arrays;

public class LoginFragment extends Fragment {
    View view;
    TextView txtForgotPassword;
    EditText etxtUserName, etxtPassword;
    Button btnSignUp, btnSignIn;
    LoginButton btnFacebookLogin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_fragment, container, false);

        AppService.getInstance().checkIfUserAlreadyLoggedIn(getContext());

        initViews();
        setViewsListeners();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppService.getInstance().callbackManager.onActivityResult(requestCode, resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    void initViews(){
        etxtUserName = view.findViewById(R.id.etxtUserName);
        etxtPassword = view.findViewById(R.id.etxtPassword);

        btnSignUp = view.findViewById(R.id.btnSignUp);
        btnSignIn = view.findViewById(R.id.btnSignIn);

        // Facebook
        btnFacebookLogin = view.findViewById(R.id.btnFacebookLogin);

        txtForgotPassword = view.findViewById(R.id.txtForgotPassword);
    }

    void setViewsListeners(){
        // Sign up
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LoginActivity) getActivity()).changeFragment(AppService.getInstance().loginActivity.SIGNUP_FRAGMENT_INDEX);
            }
        });
        // Login with Email & Password
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
        // Login via Facebook
        fbSignIn();
    }

    /*----------------------------Sign in via Email & Password-------------------------------------*/
    void signIn(){
        validateUserInputs();
        String username = etxtUserName.getText().toString();
        String password = etxtPassword.getText().toString();
        AppService.getInstance().saveStringIntoSharedPreferences("Username", username.toLowerCase());
        AppService.getInstance().saveStringIntoSharedPreferences("Password", Tools.md5(password));
        AppService.getInstance().mFirebaseAuth
                .signInWithEmailAndPassword(AppService.getInstance().getStringFromSharedPreferences("Username")
                        , AppService.getInstance().getStringFromSharedPreferences("Password"))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d("LoginFragment", "signInWithEmail:success");
                            AppService.getInstance().mFirebaseUser = AppService.getInstance().mFirebaseAuth.getCurrentUser();
                                if(AppService.getInstance().getStringFromSharedPreferences("Username").equalsIgnoreCase("app@dynamicssimulators.com")){
                                    startActivity(new Intent(getActivity(), AdminActivity.class));
                                }else{
                                    startActivity(new Intent(getActivity(), UserActivity.class));
                                }
                        }else{
                            Log.w("LoginFragment", "signInWithEmail:failure", task.getException());
                            Toast.makeText(getContext(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    void validateUserInputs(){
        if(etxtUserName.getText().toString().isEmpty()){
            etxtUserName.setError("Username required");
            etxtUserName.requestFocus();
            return;
        }

        if(etxtPassword.getText().toString().isEmpty()){
            etxtPassword.setError("Password required");
            etxtPassword.requestFocus();
            return;
        }
    }

    /*----------------------------Sign in via Facebook-------------------------------------*/
    void fbSignIn(){
        Log.d("LoginFragment", "fbSignIn() called");
        btnFacebookLogin.setReadPermissions(Arrays.asList("email"));
        // If you are using in a fragment, call loginButton.setFragment(this);
        btnFacebookLogin.setFragment(this);
        // Callback registration
        btnFacebookLogin.registerCallback(AppService.getInstance().callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                AppService.getInstance().sharedpreferences.edit().clear();
                handleFacebookToken(loginResult.getAccessToken());
                // Check if user info is added
                // Intent to user activity
                Log.d("LoginFragment", "fbSignIn() onSuccess()");
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("LoginFragment", "fbSignIn() onError: " + exception);
            }
        });
    }

    void handleFacebookToken(AccessToken accessToken){
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        AppService.getInstance().mFirebaseAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    AppService.getInstance().mFirebaseUser = AppService.getInstance().mFirebaseAuth.getCurrentUser();
                    // TODO: Check if user mail exists
                    checkIfFacebookMailExistsInDB();
                }else {
                    Toast.makeText(getActivity(), "Something gone wrong!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

   public void checkIfFacebookMailExistsInDB(){
        AppService.getInstance().mDocumentReference = AppService.getInstance().mFireStore.collection("Users").document(AppService.getInstance().mFirebaseUser.getEmail());
        AppService.getInstance().mDocumentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("LoginFragment", "checkIfFacebookMailExistsInDB() --> Listen failed: ", e);
                    return;
                }
                if (documentSnapshot != null && documentSnapshot.exists()) {
                    Log.d("LoginFragment", "Current data: " + documentSnapshot.getData());
                    startActivity(new Intent(getActivity(), UserActivity.class));
                } else {
                    Log.d("LoginFragment", "Current data: null");
                    ((LoginActivity) getActivity()).changeFragment(AppService.getInstance().loginActivity.SIGNUP_FRAGMENT_INDEX);
                }
            }
        });
    }
}
