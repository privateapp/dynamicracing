package android.com.dynamicracing.Login;

import android.com.dynamicracing.Global.AppService;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private SignUpFragment signUpFragment;
    private VerificationFragment verificationFragment;
    private ResetPasswordFragment resetPasswordFragment;
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.w("ViewPagerAdapter", "getItem() --> position: " + position);
        if(position == 0){
            if(AppService.getInstance().loginFragment == null)
                AppService.getInstance().loginFragment = new LoginFragment();
            return AppService.getInstance().loginFragment;
        }else if(position == 1){
            if(signUpFragment == null)
                signUpFragment = new SignUpFragment();
            return signUpFragment;
        }
        else if(position == 2){
            if(verificationFragment == null)
                verificationFragment = new VerificationFragment();
            return verificationFragment;
        }
        else if(position == 3){
            if(resetPasswordFragment == null)
                resetPasswordFragment = new ResetPasswordFragment();
            return resetPasswordFragment;
        }else {
            return null;
        }

    }

    @Override
    public int getCount() {
        return 4;
    }
}
