package android.com.dynamicracing.AdminActivity;

import android.com.dynamicracing.Classes.Users;
import android.com.dynamicracing.Global.AppService;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.com.dynamicracing.R;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class RanksOfAdminFragment extends Fragment {
    View view;
    ListView listView;
    CustomAdapter customAdapter;
    public RanksOfAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.admin_ranks_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        customAdapter = new CustomAdapter();
        updateListView();
        return view;
    }

    void updateListView(){
        AppService.getInstance().mFireStore.collection("Users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                AppService.getInstance().racerMap.clear();
                for (DocumentSnapshot snapshot : documentSnapshots){
                    if(!snapshot.getString("email").equalsIgnoreCase("app@dynamicssimulators.com")) {
                        Users users = new Users();
                        users.nickName = snapshot.getString("nick_name");
                        users.userID = snapshot.getString("userID");
                        users.rank = snapshot.getString("rank");
                        AppService.getInstance().racerMap.put(snapshot.getString("email"),users);
                    }
                }
                AppService.getInstance().racerMap = sortByValue(AppService.getInstance().racerMap);
                listView.setAdapter(customAdapter);
                customAdapter.notifyDataSetChanged();
            }
        });
    }

    private Map<String, Users> sortByValue(Map<String, Users> unsortMap) {
        // 1. Convert Map to List of Map
        List<Map.Entry<String, Users>> list =
                new LinkedList<Map.Entry<String, Users>>(unsortMap.entrySet());
        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Users>>() {
            public int compare(Map.Entry<String, Users> o1,
                               Map.Entry<String, Users> o2) {
                return (o1.getValue().rank).compareTo(o2.getValue().rank);
            }
        });
        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Users> sortedMap = new LinkedHashMap<String, Users>();
        for (Map.Entry<String, Users> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        /*
        //classic iterator example
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }*/
        return sortedMap;
    }

    class CustomAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        Object[] usersIDArray;
        public CustomAdapter()
        {
            mInflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            usersIDArray = new String[AppService.getInstance().racerMap.size()];
            usersIDArray = AppService.getInstance().racerMap.keySet().toArray();
            // TODO Auto-generated method stub
            return AppService.getInstance().racerMap.size();//listview item count.
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            convertView = mInflater.inflate(R.layout.custom_row_layout_of_listview, parent,false);
            //inflate custom layout
            RelativeLayout rloRowContainer = (RelativeLayout) convertView.findViewById(R.id.rloRowContainer);
            TextView txtNumOfUser= (TextView)convertView.findViewById(R.id.txtNumOfUser);
            TextView txtNickName= (TextView)convertView.findViewById(R.id.txtNickName);
            TextView txtID= (TextView)convertView.findViewById(R.id.txtID);
            TextView txtTime= (TextView)convertView.findViewById(R.id.txtTime);
            TextView txtEmail= (TextView)convertView.findViewById(R.id.txtEmail);

            txtNumOfUser.setText(position+1 + "- ");
            txtNickName.setText(AppService.getInstance().racerMap.get(usersIDArray[position]).nickName);
            txtID.setText(AppService.getInstance().racerMap.get(usersIDArray[position]).userID);
            txtTime.setText("Time: " + AppService.getInstance().racerMap.get(usersIDArray[position]).rank);
            txtEmail.setText("Email: " + usersIDArray[position].toString());

            rloRowContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showUserRankDialog(position);
                }
            });

            return convertView;
        }

        void showUserRankDialog(final int position){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Set Time");

            final EditText input = new EditText(getActivity());
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    updateData(position, input.getText().toString());
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }
        private void updateData(int position, String input) {
            DocumentReference contact = AppService.getInstance().mFireStore.collection("Users").document(usersIDArray[position].toString());
            contact.update("rank", input).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });
        }
    }
}
