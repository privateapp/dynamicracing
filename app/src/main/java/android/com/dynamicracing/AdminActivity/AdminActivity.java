package android.com.dynamicracing.AdminActivity;

import android.com.dynamicracing.R;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class AdminActivity extends AppCompatActivity {
    private FrameLayout mainFrameLayout;
    BottomNavigationView bottomNavigation;

    private RanksOfAdminFragment ranksOfAdminFragment;
    private InfoOfAdminFragment infoOfAdminFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_activity);

        initViews();
        setViewsAction();
        setFragment(ranksOfAdminFragment);
    }
    void initViews(){
        mainFrameLayout = findViewById(R.id.mainFrameLayout);

        ranksOfAdminFragment = new  RanksOfAdminFragment();
        infoOfAdminFragment = new InfoOfAdminFragment();

        bottomNavigation = findViewById(R.id.bottomNavigationView);
    }

    void setViewsAction(){
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.admin_navigation_ranks:
                        setFragment(ranksOfAdminFragment);
                        return true;
                    case R.id.admin_navigation_Info:
                        setFragment(infoOfAdminFragment);
                        return true;
                }
                return false;
            }
        });
    }

    private void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment);
        fragmentTransaction.commit();
    }
}
