package android.com.dynamicracing.AdminActivity;


import android.app.Notification;
import android.app.NotificationManager;
import android.com.dynamicracing.Classes.Users;
import android.com.dynamicracing.Global.AppService;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.com.dynamicracing.R;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoOfAdminFragment extends Fragment {
View view;
TextView txtNumOfAllUsers;
int totalNumOfUsers;

    public InfoOfAdminFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.admin_info_fragment, container, false);
        txtNumOfAllUsers = (TextView)view.findViewById(R.id.txtNumOfAllUsers);
        getNumOfAllUsers();
        return view;
    }

    void getNumOfAllUsers(){
        AppService.getInstance().mFireStore.collection("Users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                totalNumOfUsers= 0;
                for (DocumentSnapshot snapshot : documentSnapshots){
                    if(!snapshot.getString("email").equalsIgnoreCase("app@dynamicssimulators.com")) {
                        totalNumOfUsers += 1;
                    }
                }
                pushNotificationOnDBChanges();
                txtNumOfAllUsers.setText("Total users: " + totalNumOfUsers);
            }
        });
    }

    void pushNotificationOnDBChanges(){
        NotificationManager notif=(NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify=new Notification.Builder
                (getApplicationContext()).setContentTitle("Update").setContentText("New user added!").setSmallIcon(R.drawable.car_logo).setAutoCancel(true).build();

        notif.notify(0, notify);
    }
}
